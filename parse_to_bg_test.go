package mdsp_task

import (
	"fmt"
	"testing"
)

func TestParseAnswer(t *testing.T) {

	json_1 := []byte(`
    {
    "hash" : "sdsdsdkie5h65v6j465vg5j4e",
    "host" : {
      "lib" : "sdsds",
        "ip" : "192.168.1.1",
        "port" : 225
    },
    "device" : {
        "id" : 100,
        "lib" : "device_lib",
          "serial" : "sdsds",
          "num485" : 50
      },
    "repeaters" : [
      {
        "lib" : "sdsds",
          "num485" : 232,
          "serial" : "sdsds"
      }
    ],
    "year" : 215,
    "month" : 34343,
    "day" : 3232,
    "hour" : 152,
    "min" : 10,
    "cmd" : "wewew"
  }
  `)

	task_to_bg := ParseAnswerTaskToBg(json_1)

	task_to_bg.AddValue(&Value{Data: "300", Parameter: "sdsdsds"})
	task_to_bg.AddValue(&Value{Data: "ASASA", Parameter: "sdsdsds2"})

	task_to_core := ParseAnswerTaskCore(task_to_bg.ToCoreJson())

	fmt.Println(task_to_core)

}

func TestCreateMsgToCoreGetTask(t *testing.T) {
	if `{"cmd":"get_task"}` != string(GetTaskCmd()) {
		t.Error("invalid command")
	}
}
