package mdsp_task

import (
	"testing"
)

func TestGetTime(t *testing.T) {
	v := Value{
		Year:  2019,
		Month: 2,
		Day:   3,
		Hour:  4,
		Min:   5,
		Sec:   6,
	}

	if v.GetTime().String() != "2019-02-03 04:05:06 +0500 +05" {
		t.Fatal("dfdf")
	}
}
