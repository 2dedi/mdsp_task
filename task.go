package mdsp_task

/*

  Format task to bg
  -----------------

    hash  : string
    year  : int
    month : int
    day   : int
    hour  : int
    min   : int
    sec   : int
    cmd   : string
    host :
      lib       : string
      ip        : string
      port      : int
    device :
      id        : int
      lib       : string
      serial    : string
      num485    : int
    repeaters : [
      lib       : string
      num485    : int
      serial    : string
    ]



  Format task to core
  -------------------

    hash  : string
    cmd   : string
    id    : int
    values : [
      data      : string
      parameter : string
    ]

*/

import (
	"encoding/json"
	"time"
)

const (
	CMD_GET_TASK = "get_task"
	CMD_GET_DATA = "get_data"
	CMD_END      = "end"
	CMD_SUCCESS  = "success"
	CMD_TIMEOUT  = "timeout"
)

type IfaceDevice interface {
	GetLibrary() string
	GetSerial() string
	GetPass() string
	GetNum485() int
	GetId() int
}

type IfaceConcentrator interface {
	GetLibrary() string
	GetSerial() string
	GetAddr() int
	GetId() int
}

type IfaceConverter interface {
	GetLibrary() string
	GetIp() string
	GetPort() int
	GetId() int
}

type Host struct {
	Lib  string `json:"lib"`
	Ip   string `json:"ip"`
	Port int    `json:"port"`
	Id   int    `json:"id"`
}

type Device struct {
	Lib    string `json:"lib"`
	Serial string `json:"serial"`
	Pass   string `json:"pass"`
	Num485 int    `json:"num485"`
	Id     int    `json:"id"`
}

type Repeater struct {
	Lib    string `json:"lib"`
	Serial string `json:"serial"`
	Num485 int    `json:"num485"`
	Id     int    `json:"id"`
}

type Value struct {
	Data      string `json:"data"`
	Parameter string `json:"parameter"`
	Year      int    `json:"year"`
	Month     int    `json:"month"`
	Day       int    `json:"day"`
	Hour      int    `json:"hour"`
	Min       int    `json:"min"`
	Sec       int    `json:"sec"`
}

type TaskToBg struct {
	Year     int    `json:"year"`
	Month    int    `json:"month"`
	Day      int    `json:"day"`
	Hour     int    `json:"hour"`
	Min      int    `json:"min"`
	Sec      int    `json:"sec"`
	Cmd      string `json:"cmd"`
	Host     `json:"host"`
	Device   `json:"device"`
	Repeater `json:"repeater"`
	Values   []*Value `json:"values"`
}

// Тут данные а какой период было запрошено
type TaskToCore struct {
	Cmd    string   `json:"cmd"`
	Id     int      `json:"id"`
	Values []*Value `json:"values"`
}

type TaskToCoreSuccess struct {
	Id  int
	Cmd string
}

type TaskToCoreGetTask struct {
	Cmd string `json:"cmd"`
}

func NewTaskToBg() *TaskToBg {
	t := &TaskToBg{Year: time.Now().Year(),
		Month: int(time.Now().Month()),
		Day:   time.Now().Day(),
		Hour:  time.Now().Hour(),
		Min:   time.Now().Minute(),
		Sec:   time.Now().Second()}
	return t
}

func NewTaskToCoreSuccess() *TaskToCoreSuccess {
	return &TaskToCoreSuccess{}
}

func NewDevice() *Device {
	return &Device{}
}

func NewRepeater() *Repeater {
	return &Repeater{}
}

func (t *TaskToBg) SetDevice(d *Device) {
	t.Device = *d
}

func NewHost() *Host {
	return &Host{}
}

func (t *TaskToBg) SetHost(h *Host) {
	t.Host = *h
}

func (t *TaskToBg) SetRepeater(h *Repeater) {
	t.Repeater = *h
}

func NewTaskToCoreGetTask() *TaskToCoreGetTask {
	return &TaskToCoreGetTask{}
}

func NewTaskToCore() *TaskToCore {
	return &TaskToCore{}
}

func ParseAnswerTaskToBg(answer []byte) *TaskToBg {
	t := &TaskToBg{}
	err := json.Unmarshal(answer, t)
	if err != nil {
		return nil
	}
	return t
}

func ParseAnswerTaskCore(answer []byte) *TaskToCore {
	t := &TaskToCore{}
	err := json.Unmarshal(answer, t)
	if err != nil {
		return nil
	}
	return t
}

// Преобразование для отправки в ядро
func (t *TaskToBg) ToCoreJson() []byte {
	task := &TaskToCore{Id: t.Device.Id, Cmd: t.Cmd, Values: t.Values}
	res, err := json.Marshal(task)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

func (t *TaskToBg) ToCoreTimeoutJson() []byte {
	task := &TaskToCore{Id: t.Device.Id, Cmd: CMD_TIMEOUT, Values: t.Values}
	res, err := json.Marshal(task)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

// Добавление параметра в посылку
func (t *TaskToBg) AddValue(v *Value) {
	t.Values = append(t.Values, v)
}

// Формирование команды для получения задания
func GetTaskCmd() []byte {
	task := &TaskToCoreGetTask{Cmd: CMD_GET_TASK}
	res, err := json.Marshal(task)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

func (t *TaskToBg) SendParameter(parameter, value string,
	year, month, day, hour, min, sec int) []byte {
	v := &Value{Parameter: parameter, Data: value,
		Year:  year,
		Month: month,
		Day:   day,
		Hour:  hour,
		Min:   min,
		Sec:   sec}
	task := &TaskToCore{Id: t.Device.Id, Cmd: t.Cmd, Values: []*Value{v}}
	res, err := json.Marshal(task)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

func (t *TaskToBg) SendSuccess() []byte {
	task := &TaskToCoreSuccess{Id: t.Host.Id, Cmd: CMD_SUCCESS}
	res, err := json.Marshal(task)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

// Получение времени исходя из пришедших данных
func (v *Value) GetTime() time.Time {
	return time.Date(v.Year, time.Month(v.Month), v.Day, v.Hour, v.Min, v.Sec, 0, time.Now().Location())
}

func (t *TaskToBg) ToJSON() []byte {
	res, err := json.Marshal(t)
	if err != nil {
		return make([]byte, 0)
	}
	return res
}

// Добавление конвертера в сообщение
func (msg *TaskToBg) ConverterToTask(one_converter IfaceConverter) {
	host := NewHost()
	host.Id = one_converter.GetId()
	host.Ip = one_converter.GetIp()
	host.Port = one_converter.GetPort()
	host.Lib = one_converter.GetLibrary()

	if host.Lib == "" {
		return
	}

	msg.SetHost(host)
}

// Добавление концентратора в сообщение
func (msg *TaskToBg) ConcentratorToTask(one_concentrator IfaceConcentrator) {
	concentrator := NewRepeater()
	concentrator.Lib = one_concentrator.GetLibrary()
	concentrator.Serial = one_concentrator.GetSerial()
	concentrator.Num485 = one_concentrator.GetAddr()
	concentrator.Id = one_concentrator.GetId()

	if concentrator.Lib == "" {
		return
	}
	msg.SetRepeater(concentrator)
}

// Добавление электросчетчика в сообщение
func (msg *TaskToBg) EcounterToTask(one_ecounter IfaceDevice) {
	device := NewDevice()
	device.Lib = one_ecounter.GetLibrary()
	device.Serial = one_ecounter.GetSerial()
	device.Pass = one_ecounter.GetPass()
	device.Num485 = one_ecounter.GetNum485()
	device.Id = one_ecounter.GetId()

	if device.Id == 0 {
		return
	}
	msg.SetDevice(device)
}

// Добавление счетчика импульсов в сообщение
func (msg *TaskToBg) IcounterToTask(one_ecounter IfaceDevice) {
	device := NewDevice()
	device.Lib = one_ecounter.GetLibrary()
	device.Serial = one_ecounter.GetSerial()
	device.Pass = one_ecounter.GetPass()
	device.Num485 = one_ecounter.GetNum485()
	device.Id = one_ecounter.GetId()

	if device.Id == 0 {
		return
	}
	msg.SetDevice(device)
}
